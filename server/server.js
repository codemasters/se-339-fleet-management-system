const express = require('express'),
	app = express(),
	port = process.env.PORT || 8080,
	path = require('path'),
	http = require('http'),
	router = express.Router();
	mongoose = require('mongoose'),
	Example = require('./api/models/exampleModel'),
	Manager = require('./api/models/managerModel'),
	Vehicle = require('./api/models/vehicleModel'),
	bodyParser = require('body-parser'),
	mongodb = require('mongodb'),
	nconf = require('nconf'),
	absolutePath = '/home/vm-user/se-339-fleet-management-system/www/client_side',
	server = true;

if(server)
{
	nconf.argv().env().file('keys.json');
}
/*
const user = nconf.get('mongoUser');
const pass = nconf.get('mongoPass');
const host = nconf.get('mongoHost');
const port = nconf.get('mongoPort');


let uri = `mongodb://${user}:${pass}@${host}:${port}`;
if (nconf.get('mongoDatabase')) {
	  uri = `${uri}/${nconf.get('mongoDatabase')}`;
}
console.log(uri);
*/
uri = 'mongodb://fleet:manager@ds259855.mlab.com:59855/fleetdb'
urilocal = 'mongodb://localhost/exampledb'
mongoose.Promise = global.Promise;
if(server)
{
	mongoose.connect(uri, { useMongoClient: true });
}
else{
	mongoose.connect(urilocal, { useMongoClient: true });
}
app.use(bodyParser.urlencoded({extended: true }));
app.use(bodyParser.json());

//  Serve frontend view
app.use(express.static(absolutePath));

/*
router.get('/api', (req, res) => {
    res.status(200).send({message: 'Hello World!'})
});
*/

app.use('/home', (req, res) => {
	res.sendFile(absolutePath + '/fleetmap.html');
});
app.use('/editFleet', (req, res) => {
	res.sendFile(absolutePath + '/editFleet.html');
});
app.use('/editView', (req, res) => {
	res.sendFile(absolutePath + '/editView.html');
});
app.use('/login', (req, res) => {
	res.sendFile(absolutePath + '/login.html');
});
app.use('/register', (req, res) => {
	res.sendFile(absolutePath + '/register.html');
});
app.use('/logout',(req, res)  => {
	res.sendFile(absolutePath + '/logout.html')
});

app.use(router);

var routes = require('./api/routes/exampleRoute');
routes(app);

app.set('port', port);

const actualServer = http.createServer(app);

actualServer.listen(port, () => console.log('Running on http://se329-02.ece.iastate.edu:8080'));