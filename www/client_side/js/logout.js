function logoutCtrl($scope, SessionService) {
    SessionService.clearSession();
    window.location = "/login";
}

angular
    .module('logout', ['SessionService'])
    .controller('logoutCtrl', logoutCtrl);