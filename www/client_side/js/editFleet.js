function editFleetCtrl($scope, $http, $interval, NavbarService, SessionService){
    NavbarService.initializeNavbar($scope);
    $scope.username = SessionService.getCurrentUser();
    $scope.vehicleIDs = [];

    $scope.getVehicleIDs = function(){
        var url = "http://se329-02.ece.iastate.edu:8080/manager/" + $scope.username;
        //console.log("The url is: " + url);
        $.get(url, function(data, status){
            $scope.vehicleIDs = [];
            for(i = 0; i < data.length; i++){
                //console.log("data.length is: " + data.length);
                if(data[i].vid != null){
                    $scope.vehicleIDs.push(data[i].vid);
                }
            }
        });
    }

    $interval(function(){
        $scope.getVehicleIDs();
    }, 500);

    $scope.delete = function(vehicleID){
        var url = "http://se329-02.ece.iastate.edu:8080/vehicle/" + vehicleID; //TODO
        $.ajax({
            type: 'DELETE',
            url: url
        }).done(function() {
             alert("Deleted " + vehicleID);
            });
    }

    $scope.add = function(){
        var url = "http://se329-02.ece.iastate.edu:8080/manager/" + $scope.username;
        //console.log("Data is: " + $scope.addID, $scope.addBitrate, $scope.addGasTankSize);
        $.ajax({
            type: 'PUT',
            url: url,
            data: { vid: $scope.addID, bitrate: $scope.addBitrate, gasTankSize: $scope.addGasTankSize }
        }).done(function() {
             alert("Added " + $scope.addID);
            });
    }
}

angular
  .module('editFleet', ['NavbarService', 'SessionService'])
  .controller('editFleetCtrl', editFleetCtrl);
