function navbarCtrl($scope, SessionService){
    if(SessionService.getCurrentUser() == null){
        $scope.showHome = false;
        $scope.showEditFleet = false;
        $scope.showEditView = false;
        $scope.showLogin = true;
        $scope.showRegister = true;
    }else{
        $scope.showHome = true;
        $scope.showEditFleet = true;
        $scope.showEditView = true;
        $scope.showLogin = false;
        $scope.showRegister = false;
    }

    $scope.home.clicked = function() {
        window.location = "#/home";
    }
    $scope.editFleet.clicked = function() {
        window.location = "#/editFleet";
    }
    $scope.editView.clicked = function() {
        window.location = "#/editView";
    }
    $scope.login.clicked = function() {
        window.location = "#/login";
    }
    $scope.register.clicked = function() {
        window.location = "#/register";
    }
    $scope.logout.clicked = function() {
        SessionService.clearSession();
        window.location.href = "#/login";
    }
}//End controller

angular
  .module('navbar', ['SessionService'])
  .controller('navbarCtrl', navbarCtrl);
